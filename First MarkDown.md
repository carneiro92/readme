# 

Mohon maaf dikarenakan perangkat laptop yang saya gunakan mengalami kerusakan dan harus di service sampai 2 hari maka saya mengalami keterlambatan mengerjakan tugas ini.

**Project ini masih terus saya buat dan belum sampai  selesai (masih butuh untuk membuat image service dan video service)**


# Architecture

![enter image description here](https://firebasestorage.googleapis.com/v0/b/mechat-f3ab1.appspot.com/o/phully%20architecture.png?alt=media&token=7639935c-c60d-4dd1-bc7c-e8b7779e3adf)
## Peng
Dikarenakan kebijakan heroku (free hosting) yang membuat service (app) berada dalam status sleep jika tidak digunakan dalam jangka waktu tertentu maka sebelum digunakan harap untuk mengunjungi setiap url service berikut secara berurutan (mulai dari 1.discovery service hingga 5.Front End (ReactJS)) untuk membuat setiap service dalam keadaan up. Service status dapat dilihat dibagian discovery service (instances currently registered with Eureka) seharusnya akan ada 6 service yang terdaftar dengan catatan (2 discovery service) .
status semua service yang up dapat dilihat seperti contoh gambar berikut :

![enter image description here](https://firebasestorage.googleapis.com/v0/b/meteacher-762b7.appspot.com/o/Untitled.png?alt=media&token=8984a747-7ad9-480d-a4d4-14a307131a33)

(gambar diambil dari (https://discovery-service-phully-001.herokuapp.com))

Status up didapatkan dengan mengunjungi semua url dibawah secara berurutan hingga muncul response dari server atau muncul form login

  1. Discovery Service

| Service Name | URL                                                 |
| ------------ | --------------------------------------------------- |
| tata         | https://discovery-service-phully-001.herokuapp.com/ |
| titi-2       | https://discovery-service-phully-002.herokuapp.com/ |

  2. Auth Service

| Service Name    | URL                                            |
| --------------- | ---------------------------------------------- |
| service a table | https://user-service-phully-001.herokuapp.com/ |

  3. Resource Service 

| Service Name     | URL                                                |
| ---------------- | -------------------------------------------------- |
| movie-service    | https://movie-service-phully-001.herokuapp.com/    |
| comment-service  | https://comment-service-phully-001.herokuapp.com/  |
| schedule-service | https://schedule-service-phully-001.herokuapp.com/ |


  4. Proxy Service (Gateway) 

| Service Name  | URL                                             |
| ------------- | ----------------------------------------------- |
| proxy-service | https://proxy-service-phully-001.herokuapp.com/ |

  5. Front End (ReactJS) 

| Service Name | URL                                       |
| ------------ | ----------------------------------------- |
| memovie      | https://memovie-phully-001.herokuapp.com/ |

## Front End (ReactJS)

Untuk front end react banyak kode yang belum di hapus karena memang masih dalam tahap pengembangan, untuk melakukan penukaran token JWT Oauth2 (Authorization ) bisa menekan tombol login dan dengan memasukan info login berikut :

| username | password |
| -------- | -------- |
| admin    | admin    |
| user     | user     |

Token yang didapat (access_token dan refresh_token) tersimpan di local storage browser dan info login nama didapatkan dari decode access_token.

## Demo MeMovie
![enter image description here](https://firebasestorage.googleapis.com/v0/b/mechat-f3ab1.appspot.com/o/foto3.png?alt=media&token=df68e24e-26ad-4edf-8eec-375a8b22def6)